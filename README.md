# PrimaryForests
The project "Primary forests and protected areas in Belarus and Germany: improved understanding and conservation through joint research and collaboration"
##### Первобытные леса
###### Проект "Первобытные леса и охраняемые территории в Беларуси и Германии: улучшенное понимание и сохранение благодаря совместным исследованиям и сотрудничеству"
***
## Настройка сервера
Используется Ubuntu Server 20.04 LTS
1. Начальные установки сервера
- sudo su
- apt update
- apt upgrade
- apt install mc
2. Устанавливаем Java (Openjdk 11)
- apt install openjdk-11-jdk
3. Устанавливаем Geoserver
 ***
 ## GIS Database Structure
 - id - unique identifier (it necessary for web-portal)
 - type - type of primary forests (attributive domen)
 - forestry - forest enterprise (attributive domen)
 - department - branch of the forest enterprise (attributive domen)
 - category - category of forest (attributive domen)
 - land - category of forest land (attributive domen)
 - area 
 - species - main forest species (attributive domen)
 - bonitet - (attributive domen)
 - wetlands - true or false
 - snags - % of snags and dead-standing trees
 - protect - name of protected territories (attributive domen)
 - height 
 - age
 - cover - % of crown cover
 - 
